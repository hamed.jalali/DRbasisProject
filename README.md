# Project Title

Computation of the Demmler-Reinsch basis.

## Description

The Demmler-Reinsch basis is provided for a given smoothness degree in a uniform grid. The method assumes the grid is equidistant. Missing values are not supported.

### Prerequisites

It has been prepared by using "Armadillo" library in C/C++. So, R\Rstudio should be installed before. 



### Installing

For using this code in Windows, the following steps are required:
 - Install R\Rstudio
 - Install Rcpp package in R
 - Install RcppArmadillo package in R

### Usage
The main function is drbasis(nn,qq) with two parameters:
 - nn: Number of design points in the uniform grid. 
 - qq: Smoothness degree of the basis.

### Outputs
A list object containing the following information:

 - eigenvalues: estimadted eigenvalues
 - eigenvectors: estimated eigenvectors
 - eigenvectorsQR: orthonormal eigenvectors
 - x: equidistant grid used to build the basis

### An example

Rcpp::List Basis = drbasis(500,5); 

mat Phi_mat = Basis["eigenvectorsQR"];

vec eta_vec = Basis["eigenvalues"];



## Authors

* **Hamed Jalali** - *Advanced Scientific Computing Project* - [DRbasisProject](https://gitlab.gwdg.de/hamed.jalali/DRbasisProject.git)


## References

Rosales F. (2016). 
Empirical Bayesian Smoothing Splines for Signals with Correlated Errors: Methods and Applications

Serra, P. and Krivobokova, T. (2015)
Adaptive Empirical Bayesian Smoothing Splines

## Acknowledgments
 
* Dr.Francisco Rosales
